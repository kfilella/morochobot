<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEdicionFotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edicion_fotos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('edicion_id')->index()->nullable();
            $table->foreign('edicion_id')->references('id')->on('ediciones')->onDelete('cascade');
            $table->text('photo_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edicion_fotos');
    }
}
