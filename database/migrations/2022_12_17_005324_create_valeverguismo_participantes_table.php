<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValeverguismoParticipantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('valeverguismo_participantes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('valeverguismo_id')->index()->nullable();
            $table->foreign('valeverguismo_id')->references('id')->on('valeverguismos')->onDelete('cascade');
            $table->unsignedInteger('participante_id')->index()->nullable();
            $table->foreign('participante_id')->references('id')->on('participantes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valeverguismo_participantes');
    }
}
