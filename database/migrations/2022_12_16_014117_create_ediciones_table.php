<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEdicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ediciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->unsignedInteger('anio');
            $table->unsignedInteger('ganador_id')->index()->nullable();
            $table->foreign('ganador_id')->references('id')->on('participantes')->onDelete('cascade');
            $table->unsignedInteger('segundo_id')->index()->nullable();
            $table->foreign('segundo_id')->references('id')->on('participantes')->onDelete('cascade');
            $table->unsignedInteger('tercero_id')->index()->nullable();
            $table->foreign('tercero_id')->references('id')->on('participantes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ediciones');
    }
}
