<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNominadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nominados', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('edicion_id')->index()->nullable();
            $table->foreign('edicion_id')->references('id')->on('ediciones')->onDelete('cascade');
            $table->unsignedInteger('categoria_id')->index()->nullable();
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');
            $table->unsignedInteger('participante_id')->index()->nullable();
            $table->foreign('participante_id')->references('id')->on('participantes')->onDelete('cascade');
            $table->unsignedInteger('votos')->default(0);
            $table->boolean('ganador')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nominados');
    }
}
