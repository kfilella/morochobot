<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValeverguismosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('valeverguismos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('edicion_id')->index()->nullable();
            $table->foreign('edicion_id')->references('id')->on('ediciones')->onDelete('cascade');
            $table->unsignedInteger('categoria_id')->index()->nullable();
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');
            $table->text('titulo');
            $table->text('descripcion');
            $table->unsignedInteger('estrellas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valeverguismos');
    }
}
