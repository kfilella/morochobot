<?php

use Illuminate\Database\Seeder;

class GarciasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('garcias')->insert([
            'deuda' => 0.00
        ]);
    }
}
