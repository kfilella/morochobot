<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class FrasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $frases = [
            'Ahora resulta que TODOS ven basquet y TODOS son Heat, novelería 100%. Apuesto que sólo conocen a Lebron James',
            'Consigue oficio mamaverga',
            'Ándate a la verga que programaste un bot con mis frases, que chucha te pasa xono mamaverga',
            'LICHTSTEINER PEGALE AL ARCO MIERDA',
            'Federer no es material top 10',
            'Yo no sé que es peor: "Hacer deberes muerto de calor para mañana 7 am" o escuchar de fondo la discoteca gay PLAIN BAR que está por mi casa..',
            'GIANLUIGI BUFFON SE VA A QUEDAR SIN MUNDIAL por una sarta de muertos.',
            '@trezegoldavid David! Desde Ecuador, eres de mis ídolos! Grité todos tus goles en la Juve, que te vaya bien en Argentina!',
            'No me van a hacer ir cabreado chucha, se juegan el scudetto la puta madre!!',
            'Que gol mas de a verga la puta madre!!! Que chucha fue eso!!',
            '@El_fijo @JJGB7 @JoseLuque Sres una cosa: Anoten números, pins, mails o lo que sea, pero NO SOY SUCURSAL chucha!!',
            'Maldita sea la madre que paro a Borriello, a los agentes, a Padoin, a los esquemas de mierda, la terquedad de Conte, todo,se lo buscaron.OFF',
            'Maldita mierda, el proximo cojudo que venga a andar poniendo mariconadas en mi Twitter le saco la reverenda puta, a mi no me hacen webadas',
            'En el mar la vida es más sabrosa.',
        ];
        foreach ($frases as $frase) {
            DB::table('frases')->insert([
                'mensaje' => $frase
            ]);
        }
    }
}
