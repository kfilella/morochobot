<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Open AI Api Key
    |--------------------------------------------------------------------------
    |
    | Open AI Api Key, careful with this.
    |
    */

    'openai_api_key' => env('OPENAI_API_KEY'),
    'openai_completion_api' => env('OPENAI_COMPLETION_API'),
    'openai_text_model' => env('OPENAI_TEXT_MODEL'),
    'openai_image_api' => env('OPENAI_IMAGE_API'),
    'openai_image_model' => env('OPENAI_IMAGE_MODEL'),
];
