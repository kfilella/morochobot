<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapo extends Model
{
    protected $fillable = ['mensaje'];
}
