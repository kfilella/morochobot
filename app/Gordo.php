<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gordo extends Model
{
    protected $fillable = ['mensaje'];

    const VILLERGAS_ID = 12757116;
    const TANGA_VILLERGAS = 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/tanga.jpeg?alt=media&token=459d2d1c-4c6f-4692-bf97-53600c7f05d1';
    const BANIADO = 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/Screen%20Shot%202021-07-27%20at%2014.41.02.png?alt=media&token=dd4584b2-fa38-4f15-a7ab-335c76b6502d';
}
