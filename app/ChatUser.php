<?php

namespace App;

use Log;
use Illuminate\Database\Eloquent\Model;

class ChatUser extends Model
{
    protected $fillable = [
        'name',
        'chat_id',
        'service'
    ];

    public static function saveUser($user)
    {
        if (!$user || !$user->getId() || !$user->getUsername()) {
            return null;
        }

        try {
            $savedUser = ChatUser::firstOrCreate(
                [
                    'name' => $user->getUsername(),
                    'chat_id' => $user->getId(),
                    'service' => 'telegram'
                ]
            );

            return $savedUser;
        } catch (\Exception $e) {
            Log::error($e);
            return null;
        }
    }
}
