<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gersy extends Model
{
    protected $table = 'gersy';

    protected $fillable = ['mensaje'];
}
