<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frase extends Model
{
    protected static $negrocerdo = [
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3203.JPG?alt=media&token=10d68f9f-0945-4e32-9c52-08ae798e8319',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3204.JPG?alt=media&token=d9882704-f7da-4ded-ab3c-c8700464bf2d',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3205.JPG?alt=media&token=1ba679e4-8bd4-4fb4-8789-4d5e4bb34f31',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3206.JPG?alt=media&token=4d6594d6-3fd0-444c-bf04-79731f748ee8',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3207.JPG?alt=media&token=c9f2d67d-8550-4ee7-aa07-785119738aff',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3208.JPG?alt=media&token=c20a8d57-1656-4dbc-9821-31cc9d81f5bf',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3209.JPG?alt=media&token=0f6a0ef5-ed3d-40b2-bd67-6753e1ec58fe',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3210.JPG?alt=media&token=b7bceabc-0044-48ba-9433-2e1f1bb6bc80',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3212.JPG?alt=media&token=376a44af-07bb-4cce-a4aa-6148346d9b15',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3213.JPG?alt=media&token=00c8e167-7023-412d-a084-2278d2dafb45',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3214.JPG?alt=media&token=cf3fab77-7cd3-4fd6-b900-a6d17f372927',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3215.JPG?alt=media&token=d82015df-21da-42fb-a409-aaa8f7bd7fbc',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3216.JPG?alt=media&token=2b8f3448-86f4-4c3b-a85d-55bdadc5d340',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3217.JPG?alt=media&token=5d74dd8f-a546-480c-b0a4-e8b2dda20b41',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3218.JPG?alt=media&token=cac60f93-965f-4d40-85fa-4c6dd16339b5',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3221.JPG?alt=media&token=8971a549-5f20-4c1f-b092-2acab9a9f941',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FIMG_3220.JPG?alt=media&token=d3e09984-8c7d-43a2-b7e1-1a851ff4e95f',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-04-07%2023.40.16.jpeg?alt=media&token=accbce22-8a2e-48af-8db0-f5d1ea37c619',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2F2019-04-12%2019.40.19.jpg?alt=media&token=63fe14af-9a70-4d48-9be8-6c6ec163df93',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-05-28%2021.27.11.jpeg?alt=media&token=6e9f57e4-d36c-48da-9f32-be866c9c59e7',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2F2019-05-30%2008.00.27.jpg?alt=media&token=40f7cf75-b18d-4ef5-9e28-627ad3c49a55',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-06-20%2013.25.18.jpeg?alt=media&token=68b957f6-227a-4dc8-9127-f18e7dbdacca',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2F2019-07-18%2009.16.35.jpg?alt=media&token=a0f0a5f1-3454-4430-845b-d80c1254df87',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-07-25%2009.41.36.jpeg?alt=media&token=ad5193c0-0477-4475-a843-d65d6c46d836',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-07-27%2008.07.18.jpeg?alt=media&token=24eecd9a-c8cb-47fd-a847-128f3717e8b0',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-07-28%2006.18.36.jpeg?alt=media&token=d11e4c76-fdd4-404c-94d5-a9903c331865',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-07-28%2006.18.39.jpeg?alt=media&token=9ee2446f-888c-49dc-9207-a5e14acab3a5',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-08-02%2015.12.24.jpeg?alt=media&token=b534ecd3-3478-4fc3-87ca-d5e007d408e6',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-08-08%2016.48.04.jpeg?alt=media&token=aa3fa5c0-4d92-4e7a-ae3f-012545bb4d3b',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-08-12%2008.52.47.jpeg?alt=media&token=0779c9b5-ce48-430a-87f7-b21b2e47c730',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-09-06%2005.09.51.jpeg?alt=media&token=73924707-ca4e-476e-99f5-24ab4c72aea2',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FWhatsApp%20Image%202019-09-08%20at%208.38.57%20AM.jpeg?alt=media&token=3a32e867-de61-4c59-bbf2-4530dbd2f568',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-09-08%2019.44.12.jpeg?alt=media&token=6ebac44e-7927-4108-8989-d32fc2c7589c',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FAsset%201.png?alt=media&token=5542b647-f40c-45ec-8eec-4b26f5223552',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-10-19%2009.57.58.jpeg?alt=media&token=c43606c3-c9ab-46c4-983d-d7034825c5ad',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-10-20%2018.20.48.jpeg?alt=media&token=a5cacf2c-89ec-4a13-b8bd-b33635aff5c7',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-10-23%2010.00.18.jpeg?alt=media&token=d5f9d6f0-8fd8-45ae-8244-d186fb913071',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-12-05%2005.26.02.jpeg?alt=media&token=b6a89444-600b-4e27-9152-5b24fd4dede2',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-12-15%2016.33.04.jpeg?alt=media&token=7d20ef7f-9307-41cb-8a6a-8cce595e2c1c',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2019-12-15%2016.33.08.jpeg?alt=media&token=b1716762-47d7-4801-9f6b-8db1199b20cd',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2F2020-01-06%2021.52.06.jpg?alt=media&token=cf28ebfb-1de7-4827-ae16-71075d1dfc93',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2F2020-01-06%2021.52.28.jpg?alt=media&token=e720f38d-a09e-4177-b603-59e28e1eceb2',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2F2020-01-06%2021.52.48.jpg?alt=media&token=b21cabbe-5065-4dd0-977d-0e176cba8eba',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2020-01-11%2015.47.19.jpeg?alt=media&token=ea3aafa3-0ca2-44ed-93e9-ffe48427964b',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2020-01-12%2009.15.22.jpeg?alt=media&token=ea43fc73-5f25-41c5-b234-91f50086d37d',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2F2020-05-30%2010.46.48.jpg?alt=media&token=4a1c74a6-b106-4850-8634-f2cb34167990',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2F2020-06-07%2015.44.49.jpg?alt=media&token=79504d22-1c37-4558-b020-d7f72aa327d0',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fmorocho.jpg?alt=media&token=159aa685-85f1-4e5f-b7c2-aa4606d49f3b',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fcerdo.jpeg?alt=media&token=39732e73-5ab4-4cce-bc40-caf53ad89adc',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FScreen%20Shot%202021-11-13%20at%2017.50.07.png?alt=media&token=57fbf779-45c5-4609-b4fe-d419f6b524c0',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fphoto_2022-08-18_21-41-27.jpg?alt=media&token=3933b01a-1573-431b-b36b-728aea369bab',
    ];
    protected static $blackLivesMatter = [
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/blacklivesmatter%2Faaa.jpg?alt=media&token=c503f64a-e0e9-4d3e-b85f-360747e5c4b5',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/blacklivesmatter%2F43u30o.jpg?alt=media&token=2113bbf8-72a4-423a-82bd-ef0602ef4023',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/blacklivesmatter%2F2020-06-07%2015.42.20.jpg?alt=media&token=9e82e5d1-6c81-453a-82e4-0bf4b08d1154',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2Fnegrogay.jpeg?alt=media&token=aedd6e16-5822-4b18-9f42-9f825be37bca',
    ];

    protected static $gordasos = [
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/gordasos%2F2022-12-20%2009.55.07.jpg?alt=media&token=6c2648ca-9cfa-4675-831b-1c2af94d1fa9',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/gordasos%2F2022-12-20%2009.55.19.jpg?alt=media&token=35b3e510-491e-414b-9eb3-8e54647e7bd9',
        'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/gordasos%2FSin_bigote.jpg?alt=media&token=166a37f6-887a-49ec-b83e-f9e466b9078b',
    ];

    const AUDIOS = [
        'habla' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FPochito%20-%20Habla%20lo%20que%20chucha%20quieras.mp3?alt=media&token=5aaef86f-2ef2-433d-9664-b2c35a763496',
        'cabeza' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FPochito%20-%20Que%20mierda%20tienes%20en%20la%20cabeza%20carajo.mp3?alt=media&token=c97f4d01-ba27-4c89-bcdb-7244965d1b68',
        'brunch' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FEl%20brunch%20el%20brunch%20el%20brunch%20el%20brunch.mp3?alt=media&token=f5d775c5-44ac-4ee0-9d02-6788254495d7',
        'quetepasa' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FQue%20te%20pasa%20Pocho.mp3?alt=media&token=3e10a8e3-5f0e-42a4-a6cd-a7d3d69c0f38',
        'tonteria' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FPochito%20-%20Dejate%20de%20hablar%20tanta%20tonteri%CC%81a%20carajo.mp3?alt=media&token=ed7430a1-cbcd-468f-9cee-04cda955188c',
        'losnadies' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FLos%20nadies.mp3?alt=media&token=a9e45bb1-069e-4243-aefe-92a92cb8aa14',
        'gracias' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FMuchas%20gracias.mp3?alt=media&token=a4fa015f-ce8c-4fb7-ad5f-00887156b833',
        'isabel' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2Fisabel.mp3?alt=media&token=47325bac-031d-442b-ad27-8a62afe55095',
        'bobo' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2Fbobo.mp3?alt=media&token=c3d5b39f-fbe8-431c-9fe1-4b9a1159207b',
        'cabreado' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2Fcabreado.mp3?alt=media&token=cdd264ca-e7dd-4998-a029-5b9e78e5668f',
        'callate' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2Fcallate.mp3?alt=media&token=779c57f4-148d-428a-a619-575f161ed3c3',
        'cojudo' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2Fcojudo.mp3?alt=media&token=ccd06088-d36f-46a3-a5da-df463f7a9196',
        'explico' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2Fexplico.mp3?alt=media&token=b5f02d57-2cbd-4b0e-8976-c5b54473669b',
        'farsante' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2Ffarsante.mp3?alt=media&token=e3ec9ca7-38a2-4c52-b0a8-4f21a2d0dfb8',
        'huevadas' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2Fhablando%20huevadas.mp3?alt=media&token=a1c83abf-79b6-425e-aa15-51314605bd76',
        'invente' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2Fhuevadas.mp3?alt=media&token=9f1eab41-3d81-4314-a651-78787986bc21',
        'soplagaitas' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2Fsoplagaitas.mp3?alt=media&token=6725adc6-bb52-4e71-8c87-d08041146996',
        'opinebrin' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FOpine%20brin%20grifaso.mp3?alt=media&token=60f06c65-53a1-4866-a177-ed545f1f6893',
        'capisci' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FPepa%20Borbor%20-%20Capisci%20Sopladores.mp3?alt=media&token=18411191-1235-4743-9593-c651f380ad62',
        'prostaeros' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FWachito%20-%20DELE%20PROSTAEROS.mp3?alt=media&token=984548f4-62f3-440b-a6bd-b17d876f9319',
        'sopedazo' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a-Sopedazo%20de%20pendejo.mp3?alt=media&token=4edfb9f5-8f19-4325-a6d6-bfdfbc9dabb9',
        'miserables' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FPochito%20-%20Miserables.mp3?alt=media&token=00c0d8fc-33f2-4526-9a9b-87ee74aff070',
        'silencio' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FSILENCIO%20DELINCUENTE.mp3?alt=media&token=64c575ad-6df4-4c02-9772-8085035f226e',
        'shhh' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FShhh%20silencio%20que%20Nebot%20va%20a%20hablar.mp3?alt=media&token=b8a06e89-b3cd-418b-adf0-6380137bba21',
        'ausente' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FNebot%20Saadi%20Jaime%20-%20Ausente.mp3?alt=media&token=e72f472f-784e-4e32-813a-0fa95388588c',
        'chetumadre' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FPochito%20-%20Che%20tu%20madre.mp3?alt=media&token=e31313c8-2831-4eca-9af1-be9f51a0e42a',
        'estabamosbien' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20La%20verdad%20que%20esta%CC%81bamos%20bien%20sin%20este%20par%20de%20cojudos.mp3?alt=media&token=a40848d5-997a-486e-9802-50d91178519f',
        'groserias' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepi%C3%B1a%20-%20Yo%20voy%20a%20responder%20con%20groserias.mp3?alt=media&token=7adefc58-8e56-47c9-bb85-7d53df93381c',
        'groseria' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepi%C3%B1a%20-%20Yo%20voy%20a%20responder%20con%20groserias.mp3?alt=media&token=7adefc58-8e56-47c9-bb85-7d53df93381c',
        'gloton' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepi%C3%B1a%20-%20Te%20has%20vuelto%20un%20gloton%20hermano.mp3?alt=media&token=f5dd340b-b7f4-4e90-91cd-d51cf3bed4b5',
        'callatecrv' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Chucha%20ca%CC%81llate%20careverg.mp3?alt=media&token=35275ccf-7e0f-4644-b128-91a7ecf3720e',
        'hijosdeputa' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FPochito%20-%20Entonces%20no%20son%20unos%20hijos%20de%20p.mp3?alt=media&token=602575ab-8fdf-435b-9cda-f26e7fbcc66b',
        'aquehora' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FA%20que%20hora%20acaba%20de%20hablar.mp3?alt=media&token=794a9227-2c8a-420a-9bb1-2499ffc3dfef',
        'leyendo' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FNo%20esten%20leyendo%20a%20pendejos.mp3?alt=media&token=4f9a944a-131c-4088-ab5d-2f5ecaae2d24',
        'importa' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FA%20ti%20que%CC%81%20xuxa%20te%20importa.mp3?alt=media&token=3310e105-1eb1-4a91-a876-47b46b54e24f',
        'hipocritas' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FSarta%20de%20hip%C3%B3critas.mp3?alt=media&token=4ee5b439-6588-4ddc-bd57-0602132554d7',
        'vamonos' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FYa%20va%CC%81monos%20xuxa.mp3?alt=media&token=76451f53-4206-42ab-a5b0-2dc824cd1087',
        'chupalo' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FChupalo.mp3?alt=media&token=a6f28c81-7d3f-4d7f-98e3-0ffcb30f1fb5',
        'bomba' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FBomba.mp3?alt=media&token=2a5e67f9-1cf9-49a1-87a8-3b49a873fe23',
        'quack' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FQuack.mp3?alt=media&token=1389987c-a505-45fa-8871-73046c684b66',
        'pregunta' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Esa%20pregunta%20cojuda.mp3?alt=media&token=36b3e4ed-1375-422a-987d-29b14c576ee0',
        'sichch' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Si%20chucha.mp3?alt=media&token=bdde8d51-e7c2-419f-b1db-bd3dde4cb7ad',
        'tanda' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Nunca%20he%20vista%20una%20tanda%20de%20hptas.mp3?alt=media&token=f7af45e6-297d-4a08-81fa-ebba744d056f',
        'arrechar' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarlos%20Usategui%20-%20Me%20esta%CC%81s%20haciendo%20arrechar.mp3?alt=media&token=8b08b9fe-08db-425d-bcc5-86e52b7afdac',
        'bienestabamos' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FBien%20esta%CC%81bamos%20sin%20este%20cojudo.mp3?alt=media&token=7950a235-2545-4e23-98a1-4a7ec925f11d',
        'inexperto' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Cojudo%20e%20inexperto.mp3?alt=media&token=fe231211-2a70-4638-8659-28a0dfd3b1fc',
        'vitotvo' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepi%C3%B1a%20-%20Vito%20TVO.mp3?alt=media&token=250716ae-1fe2-4ff7-a63f-dc66e35d7b90',
        'encamar' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FNo%20me%20vengas%20a%20encamar%20hvds.mp3?alt=media&token=ee4bfe29-c8d5-4b31-ab0f-5787dbdf4cdd',
        'chupalooo' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FChupalooo....mp3?alt=media&token=303e1291-f431-4085-a840-1489f2766b64',
        'escuchar'=> 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Hoy%20me%20vas%20a%20escuchar.mp3?alt=media&token=f05a9d22-b013-48d9-b27f-021984c27e4b',
        'verges' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FMe%20vale%20Verge%CC%81s.mp3?alt=media&token=75c7613c-217a-4512-93d4-c8839d0e61a0',
        'criterio' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FLuis%20Bustamante%20-%20Tu%20criterio%20nos%20vale%20v%20.mp3?alt=media&token=18a790d9-89ad-4480-95d3-75f283b73832',
        'ayyy' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FJorgito%20-%20Ayyyyy.mp3?alt=media&token=8389ce5b-654c-4cc2-ae79-c6e3b8f04e0e',
        '1000hdp' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FMil%20veces%20hdp.mp3?alt=media&token=cdc3f2db-b478-40be-910e-a1fe2b46875a',
        'comotegustavillegol' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FPero%20que%20ancha.mp3?alt=media&token=7d9a789c-21bb-4209-a968-c05ee790fd29',
        'chchmadre' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Chucha%20madre.mp3?alt=media&token=18ad35a6-6120-4e84-addb-90a5acc1e84d',
        'alaver' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FAdrogado%20-%20Yo%20lo%20mande%20a%20la%20ver.mp3?alt=media&token=dbf9f93a-21f0-4140-a1a2-4d4a3265721c',
        'descansa' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FChch%20que%20tu%20si%20que%20jodes.mp3?alt=media&token=0769884c-05be-4bee-ae05-35073c7f7d76',
        'cachudo' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FWachito%20-%20Cachudo.mp3?alt=media&token=89b25d4f-8181-4078-8e5a-aebf78e8a0d4',
        'nalgas' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20El%20vendio%20las%20nalgas.mp3?alt=media&token=0b58feaf-710b-4448-bf6c-18168056325b',
        'mariconadas' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FDe%CC%81jate%20de%20mariconadas%20Borbor.mp3?alt=media&token=19030b2e-e6cf-4f6a-b127-5dcd76c80f35',
        'dormir' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FJorgito%20-%20Ya%20dejame%20dormir.mp3?alt=media&token=618882d8-cedf-4d9f-ae5d-7cedc321b798',
        'yamiamor' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FYA%20MI%20AMOR%20YA.mp3?alt=media&token=3f0ae26c-bdab-4e8e-b22e-838df7447287',
        'nomiamor' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20No%20mi%20amor.mp3?alt=media&token=ff569ef0-7fb1-4eb5-a07e-b2b102aa6f21',
        'hastacuando' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Hasta%20cuando%20chucha.mp3?alt=media&token=c111c1aa-f67a-451c-8724-adc43b54d1d6',
        'casadeverges' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FPochito%20-%20Se%20van%20a%20la%20casa%20de%20Verge%CC%81s%20todos.mp3?alt=media&token=f432fb5a-9501-4a1a-8b30-2e0684de84d9',
        'unratito' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarlitos%20Galvez%20-%20Un%20ratito%20chucha.mp3?alt=media&token=df1b3e35-75e9-4d3a-980a-766cc41198ed',
        'perdedor' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FHaga%20Negocio%20Conmigo%20-%20Perdedor.mp3?alt=media&token=bd96ec82-26d4-452d-b434-869eca2732a6',
        'ultra' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FUltra%20de%20que%20pues.ogg?alt=media&token=d088a276-03f9-4bf6-b87c-af8608fccd80',
        'paciencia' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FPepa%20Borbor%20-%20Ten%20paciencia%20chucha.mp3?alt=media&token=0f63a0a6-62a6-42a9-9ec3-5e14682e2943',
        'cdth' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FAlfalso%20-%20Concha%20de%20tu%20hermana.mp3?alt=media&token=fc540ca7-2e22-4ee2-ac86-5ee7a3bf7d35',
        'miercoles' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FMiercoles%20de%20hombres%20duros.mp3?alt=media&token=952e147e-4540-416c-b08a-5359c8cff75d',
        'triste' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FTarrina90%20-%20Estas%20triste.mp3?alt=media&token=4d91cb52-493b-4c40-999c-092b76a408b5',
        'nohayplata' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FMilei%20-%20No%20hay%20plata.mp3?alt=media&token=a1702a5a-1967-43b6-ba76-6578ec5efd31',
        'ptm' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Puta%20madre.mp3?alt=media&token=eb126669-ef15-46d3-8d1c-721d12f93cdc',
        'chchpregunta' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Chucha%20que%20pregunta%20mas%20estupida.mp3?alt=media&token=7329b3f0-b9a7-40b2-beae-e534f1e8e9de',
        'nopendejo' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FAlfalso%20-%20No%20no%20pendejo%20no.mp3?alt=media&token=94c3cc6e-98ad-4351-b548-e8d2ab777a13',
        'jueputas' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FAdrogado%20-%20Jueputas.mp3?alt=media&token=a1f5bb2e-a11c-483e-9274-723e1b53f3f7',
        'udsabe' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FAbdaka%20-%20Usted%20sabe.mp3?alt=media&token=75303d9d-8009-4c3e-8589-c0d0de5effac',
        'miercoles2' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FMiercoles%20prende.mp3?alt=media&token=773e6023-97bc-4ad4-bb72-7a6c845425e7',
        'bulla' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FAdrogado%20-%20Que%20manera%20de%20hacer%20bulla.mp3?alt=media&token=48fd06d9-3b24-4821-b349-3ab3c6447e89',
        'levantate' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FLevantate.mp3?alt=media&token=2b6d04fe-90d6-46e9-b202-d1e0995d22e0',
        'reputa' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Chucha%20la%20grandisima%20reputa%20(Beta).mp3?alt=media&token=859213bf-4594-42fc-a78c-d07b38b0fdf9',
        'jejeje' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FJorgito%20-%20Risa.mp3?alt=media&token=f87460b9-a035-4e55-8048-37df5a5a3d59',
        'shabido' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FAbdala%20-%20Eres%20muy%20shabido%20marihuanero.mp3?alt=media&token=2b9640b8-b97a-480f-aa4f-4f4fbe676dee',
        'audaz' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Eres%20audaz.mp3?alt=media&token=3c4eff73-69e4-4ca9-9d30-1a4b635f7224',
        'cachetear' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Cachetear%20a%20este%20par.mp3?alt=media&token=d4fc54c9-abf0-4aa0-a700-27ad9896bb8a',
        'enano' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FAdrogado%20-%20Ese%20enano%20maldito.mp3?alt=media&token=b7a9c025-615c-42b1-b63e-6422902d7b37',
        'sipues' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FPochito%20-%20Si%20pues%20chch.mp3?alt=media&token=da76a7d3-f350-40ef-b7c5-883186d20504',
        'salado' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20La%CC%81rgate%20Salado.mp3?alt=media&token=cac79a99-1811-4b1c-a54a-b1fb3d2d4a8d',
        'vivaelamor' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FAlfalso%20-%20Viva%20el%20amor.mp3?alt=media&token=7e3f7a79-7d99-42d1-b38f-ca6d0e438481',
        'duelecabeza' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FAlfalso%20-%20Me%20duele%20la%20cabeza.mp3?alt=media&token=02fcb526-632b-4c92-a5e8-4f8de01a7910',
        'pepaconfirmada' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FCarepin%CC%83a%20-%20Pepa%20Confirmada.mp3?alt=media&token=69bd25e8-9dde-40a2-8ebb-d793f5602136',
    ];

    const VIDEOS = [
        'amanezcoflaco' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/videos%2FVillegol%20-%20Cualquier%20rato%20a%20a%20a%20amanezco%20flaco.MP4?alt=media&token=2aa9141e-70c6-4b35-a8df-ff0c74f70489',
        'taylorswift' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/videos%2FGordaso%20-%20%23TaylorSwiftArgentina.mp4?alt=media&token=fca61c5d-fac4-42aa-b9c4-1ad7b92186d6',
        'sonmiscosas'=> 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/videos%2FEl%20Morocho%20-%20Son%20mis%20cosas.mp4?alt=media&token=0a1a0797-b2d9-40be-b2c8-72e7abf5d8cd',
        'impresentable'=> 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/videos%2FVillegol%20-%20Impresentable.mp4?alt=media&token=d70aa4ad-f574-4623-83c6-3f057a4c4368',
        'cambioyfuera' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/videos%2FCarepi%C3%B1a%20(Villegol)%20-%20O%20cambias%2C%20o%20te%20cambian%2C%20cambio%20y%20fuera.mp4?alt=media&token=7c699a68-b5b9-429d-962e-16d231141518',
        'miercoles' => 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/videos%2FMiercoles%20de%20hombres%20duros.mp4?alt=media&token=c5f20fef-3563-49ab-b0a9-3b325066d6cc',
    ];

    const COLORES_GROUP_ID = -1001137860003;
    const WEDNESDAY_IDS = [-10022881793, -1001137860003];
    const COMUNICADO = 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/comunicado.jpeg?alt=media&token=1669b829-9a3d-456d-83dc-3618997bbf1f';
    const TAS_QUE_RONCAS_NO = 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/negrocerdo%2FTas%20que%20roncas%20no.jpg?alt=media&token=6dda6546-dbff-4efe-8f98-a3ff1dc68f7a';
    const WEDNESDAY = 'https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/TodayIsWednesday.jpg?alt=media&token=bb9d8020-14d0-4c96-9b65-69f4e7e98359';
    protected $fillable = ['mensaje'];

    public static function allCerdos()
    {
        return Frase::$negrocerdo;
    }

    public static function lastCerdo()
    {
        return end(Frase::$negrocerdo);
    }

    public static function randomCerdo()
    {
        $cerdos = Frase::$negrocerdo;
        shuffle($cerdos);
        return $cerdos[0];
    }

    public static function randomGordaso()
    {
        $image = Frase::$gordasos;
        shuffle($image);
        return $image[0];
    }

    public static function randomBlackLivesMatter()
    {
        $blackLivesMatter = Frase::$blackLivesMatter;
        shuffle($blackLivesMatter);
        return $blackLivesMatter[0];
    }

    public static function cerdoIndex($index)
    {
        $cerdos = Frase::$negrocerdo;
        $i = $index - 1;
        return array_key_exists($i, $cerdos) ? $cerdos[$i] : null;
    }

    public static function gordasoIndex($index)
    {
        $gordos = Frase::$gordasos;
        $i = $index - 1;
        return array_key_exists($i, $gordos) ? $gordos[$i] : null;
    }

    public static function countCerdos()
    {
        return count(Frase::$negrocerdo);
    }

    public static function countGordasos()
    {
        return count(Frase::$gordasos);
    }
}
