<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prensa extends Model
{
    protected $fillable = ['mensaje'];
}
