<?php

namespace App\Traits;

use App\Configuracion;
use App\Frase;
use Illuminate\Support\Facades\Log;

trait TelegramTrait
{
    public static function sendTodayIsWednesdayImage()
    {
        foreach (Frase::WEDNESDAY_IDS as $chatId) {
            try {
                $telegramToken = config('botman.telegram.token');
                $imageUrl = Frase::WEDNESDAY;
        
                $url = "https://api.telegram.org/bot".$telegramToken."/sendPhoto";
                $postData = [
                    'chat_id' => $chatId,
                    'photo' => $imageUrl,
                ];
        
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_exec($ch);
                curl_close($ch);
            } catch (\Exception $e) {
                Log::error($e);
            }
        }
    }

    public static function botarAVillergas()
    {
        $config = Configuracion::first();
        if (!$config) {
            $config = Configuracion::create([
                'botar_a_villergas_cada_min' => 0
            ]);
        }

        if (!$config->botar_a_villergas_cada_min) {
            return;
        }

        $userId = 12757116;
        $chatId = -1001137860003;
        $post = [
            'user_id' => $userId,
            'chat_id' => $chatId,
        ];
        $ch = curl_init('https://api.telegram.org/bot152580083:AAHSBQ3p9hX42P0l67M-iKtseKvhQIAzfX4/banChatMember');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        curl_close($ch);
    }
}
