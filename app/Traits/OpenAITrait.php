<?php

namespace App\Traits;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

trait OpenAITrait
{
    /**
     * Sends a prompt to OpenAI's GPT-4 API and returns the response.
     *
     * @param string $prompt
     * @return string|null
     */
    public static function queryOpenAiGpt4(string $prompt): ?string
    {
        $gptApiKey = config('openai.openai_api_key');
        $gptApi = config('openai.openai_completion_api');
        $model = config('openai.openai_text_model');
        $client = new Client();
        $responseMessage = '';
        try {
            $response = $client->post($gptApi, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $gptApiKey,
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    'model' => $model,
                    'messages' => [
                        ['role' => 'user', 'content' => json_encode($prompt)],
                    ],
                    'max_tokens' => 1000,
                ],
            ]);
            $data = json_decode($response->getBody(), true);

            if (isset($data['choices']) && count($data['choices']) > 0) {
                $responseMessage = $data['choices'][0]['message']['content'];
            } else {
                $responseMessage = 'Valió pistola OpenAI sanguito, intenta luego. Villergas vale verga.';
            }
        } catch (\Exception $e) {
            Log::error($e);
            $responseMessage = 'Valió pistola OpenAI sanguito, intenta luego. Villergas vale verga.';
        }

        return $responseMessage;
    }

    public static function queryOpenAiDalle3($prompt)
    {
        $apiKey = config('openai.openai_api_key');
        $imageAPi = config('openai.openai_image_api');
        $model = config('openai.openai_image_model');
        $client = new Client();
        try {
            $response = $client->post($imageAPi, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $apiKey,
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    'model' => $model,
                    'prompt' => $prompt,
                    'n' => 1,
                    'size' => '1024x1024'
                ],
            ]);

            $responseData = json_decode($response->getBody(), true);

            // Extracting the image URL from the response
            $imageUrl = $responseData['data'][0]['url'] ?? null;
            return $imageUrl ? ['image_url' => $imageUrl] : ['error' => 'Valió pistola OpenAI sanguito, intenta luego. Villergas vale verga.'];
        } catch (\Exception $e) {
            Log::error('$e');
            Log::error($e);
            return ['error' => 'Valió pistola OpenAI sanguito, intenta luego. Villergas vale verga.'];
        }
    }
}
