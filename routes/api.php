<?php

use App\Chapo;
use App\Frase;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/morocho', function () {
    return response()->json([
        'status' => 'Gud',
        'results' => Frase::all()->pluck('mensaje')->toArray()
    ], 200);
});

Route::get('/chapo', function () {
    return response()->json([
        'status' => 'Gud',
        'results' => Chapo::all()->pluck('mensaje')->toArray()
    ], 200);
});

Route::get('/audios', function () {
    $array = [];
    return response()->json([
        'status' => 'Gud',
        'results' => Frase::AUDIOS
    ], 200);
});

Route::get('/videos', function () {
    $array = [];
    return response()->json([
        'status' => 'Gud',
        'results' => Frase::VIDEOS
    ], 200);
});

Route::get('/negrocerdo', function () {
    $array = [];
    return response()->json([
        'status' => 'Gud',
        'results' => Frase::allCerdos()
    ], 200);
});

Route::post('/largatevillergas', function () {
    $userId = 12757116;
    $chatId = -1001137860003;
    $post = [
        'user_id' => $userId,
        'chat_id' => $chatId,
    ];
    $ch = curl_init('https://api.telegram.org/bot152580083:AAHSBQ3p9hX42P0l67M-iKtseKvhQIAzfX4/banChatMember');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    $response = curl_exec($ch);
    curl_close($ch);
});