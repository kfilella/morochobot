<?php
use App\Chapo;
use App\Frase;
use App\Gordo;
use App\Gersy;
use App\Garcia;
use App\Prensa;
use App\ChatUser;
use App\Configuracion;
use BotMan\BotMan\BotMan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\BotManController;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Attachments\Audio;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Attachments\Video;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use App\Traits\OpenAITrait;

$botman = resolve('botman');

$botman->hears('/gloton', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Audio(Frase::AUDIOS['gloton']);
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
    $bot->reply('Te has vuelto glotón hermano...');
    $attachment = new Image(Frase::cerdoIndex(1));
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
    $bot->reply('Como te anocheciste comiendo hamburguesa...');
    $attachment = new Image(Frase::cerdoIndex(5));
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
    $bot->reply('Como te anocheciste comiendo hot dog...');
    $attachment = new Image(Frase::cerdoIndex(51));
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
    $bot->reply('Como te embutiste comiendo pizza...');
    $attachment = new Image(Frase::cerdoIndex(18));
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
    $bot->reply('Tomando milkshake...');
    $attachment = new Image(Frase::cerdoIndex(19));
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
    $bot->reply('Tas que roncas no?');
    $attachment = new Image(Frase::TAS_QUE_RONCAS_NO);
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});

$botman->hears('/gloton@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Audio(Frase::AUDIOS['gloton']);
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
    $bot->reply('Te has vuelto glotón hermano...');
    $attachment = new Image(Frase::cerdoIndex(1));
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
    $bot->reply('Como te anocheciste comiendo hamburguesa...');
    $attachment = new Image(Frase::cerdoIndex(5));
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
    $bot->reply('Como te anocheciste comiendo hot dog...');
    $attachment = new Image(Frase::cerdoIndex(51));
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
    $bot->reply('Como te embutiste comiendo pizza...');
    $attachment = new Image(Frase::cerdoIndex(18));
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
    $bot->reply('Tomando milkshake...');
    $attachment = new Image(Frase::cerdoIndex(19));
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
    $bot->reply('Tas que roncas no?');
    $attachment = new Image(Frase::TAS_QUE_RONCAS_NO);
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});

$botman->hears('/apoyenplixz', function ($bot) {
    $bot->reply('Deposita sanguito para cubrir los costos de IA y AWS para MorochoBot. '.
                'Banco Pichincha, '.
                'Cuenta Corriente: 2100128061, '.
                'Nombre: Kevin Filella, '.
                'C.I.: 0920174208');
    $audio = Frase::AUDIOS['gracias'];
    $attachment = new Audio($audio);
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});

$botman->hears('/morochoimagen {prompt}', function ($bot, $prompt) {
    $chatUser = ChatUser::saveUser($bot->getUser());
    if (!$chatUser) {
        $bot->reply('Este comando está en fase de pruebas sanguito.');
        return;
    }
    if ($chatUser->name == 'negrobolonero') {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    if (!in_array($chatUser->name, ['kfilella', 'MorochoChupalo'])) {
        $bot->reply('Este comando está en fase de pruebas sanguito.');
        return;
    }

    $imageData = OpenAITrait::queryOpenAiDalle3($prompt);

    if (isset($imageData['error'])) {
        $bot->reply($imageData['error']);
        return;
    }

    $imageUrl = $imageData['image_url'] ?? null;
    if ($imageUrl) {
        $message = OutgoingMessage::create()
                    ->withAttachment(new Image($imageUrl));
        $bot->reply($message);
    } else {
        $bot->reply('Valió pistola OpenAI sanguito, intenta luego. Villergas vale verga.');
    }
});

$botman->hears('/morochotexto {prompt}', function ($bot, $prompt) {
    $chatUser = ChatUser::saveUser($bot->getUser());
    if (!$chatUser) {
        $bot->reply('Este comando está en fase de pruebas sanguito.');
        return;
    }
    if ($chatUser->name == 'negrobolonero') {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    if (!in_array($chatUser->name, ['kfilella', 'MorochoChupalo'])) {
        $bot->reply('Este comando está en fase de pruebas sanguito.');
        return;
    }
    $bot->reply(OpenAITrait::queryOpenAiGpt4($prompt));
});

$botman->hears('/video {index}', function ($bot, $index) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $audio = Frase::VIDEOS[$index] ?? null;
    if ($audio) {
        $attachment = new Video($audio);
        $message = OutgoingMessage::create()
                    ->withAttachment($attachment);
        $bot->reply($message);
    } else {
        $bot->reply('No valga bolsa brin. Estas son las opciones de video: '
        .implode(', ', array_keys(Frase::VIDEOS)));
    }
});
$botman->hears('/video@MorochoBot {index}', function ($bot, $index) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $audio = Frase::VIDEOS[$index] ?? null;
    if ($audio) {
        $attachment = new Video($audio);
        $message = OutgoingMessage::create()
                    ->withAttachment($attachment);
        $bot->reply($message);
    } else {
        $bot->reply('No valga bolsa brin. Estas son las opciones de video: '
        .implode(', ', array_keys(Frase::VIDEOS)));
    }
});
$botman->hears('/video@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Anda valiendo más bolsa que Villegol brin. El formato es /video (video). Estas son las opciones de video: '
    .implode(', ', array_keys(Frase::VIDEOS)));
});
$botman->hears('/video', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Anda valiendo más bolsa que Villegol brin. El formato es /video (video). Estas son las opciones de video: '
    .implode(', ', array_keys(Frase::VIDEOS)));
});

$botman->hears('/adivinar', function (BotMan $bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $numeroAdivinar = rand(1, 5);
    $bot->ask('Adivina un número entre 1 y 5 sanguito.', function (Answer $answer) use ($bot, $numeroAdivinar) {
        $numeroUsuario = (int)$answer->getText();

        if ($numeroUsuario === $numeroAdivinar) {
            $bot->reply('Gud');
        } else {
            $bot->reply('Lo siento, '.$bot->getUser()->getUsername().'. El número correcto era ' . $numeroAdivinar . '.');
            $bot->reply('Baraja ctm');
            $userId = 12757116;
            $luqueId = 9664495;
            $chatId = -1001137860003;
            $post = [
                'user_id' => $userId,
                'chat_id' => $chatId,
            ];
            $ch = curl_init('https://api.telegram.org/bot152580083:AAHSBQ3p9hX42P0l67M-iKtseKvhQIAzfX4/banChatMember');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            $response = curl_exec($ch);
            curl_close($ch);

            $post2 = [
                'text' => $bot->getUser()->getUsername().' botó a Villergas por no adivinar el número. Gracias por su colaboración.',
                'chat_id' => $chatId,
            ];
            $ch2 = curl_init('https://api.telegram.org/bot152580083:AAHSBQ3p9hX42P0l67M-iKtseKvhQIAzfX4/sendMessage');
            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch2, CURLOPT_POSTFIELDS, $post2);
            $response2 = curl_exec($ch2);
            curl_close($ch2);

            $attachment = new Image(Frase::COMUNICADO);
            $message = OutgoingMessage::create()
                    ->withAttachment($attachment);
            $bot->reply($message);

            if ($bot->getUser()->getUsername() == 'JoseLuque7') {
                $bot->reply('Tu también lárgate ctm');
                $luqueId = 9664495;
                $chatId = -1001137860003;
                $post3 = [
                    'user_id' => $luqueId,
                    'chat_id' => $chatId,
                ];
                $ch3 = curl_init('https://api.telegram.org/bot152580083:AAHSBQ3p9hX42P0l67M-iKtseKvhQIAzfX4/banChatMember');
                curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch3, CURLOPT_POSTFIELDS, $post3);
                $response = curl_exec($ch3);
                curl_close($ch3);
            }
        }
    });
});

$botman->hears('/adivinar@MorochoBot', function (BotMan $bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $numeroAdivinar = rand(1, 5);
    $bot->ask('Adivina un número entre 1 y 5 sanguito.', function (Answer $answer) use ($bot, $numeroAdivinar) {
        $numeroUsuario = (int)$answer->getText();

        if ($numeroUsuario === $numeroAdivinar) {
            $bot->reply('Gud');
        } else {
            $bot->reply('Lo siento, '.$bot->getUser()->getUsername().'. El número correcto era ' . $numeroAdivinar . '.');
            $bot->reply('Baraja ctm');
            $userId = 12757116;
            $luqueId = 9664495;
            $chatId = -1001137860003;
            $post = [
                'user_id' => $userId,
                'chat_id' => $chatId,
            ];
            $ch = curl_init('https://api.telegram.org/bot152580083:AAHSBQ3p9hX42P0l67M-iKtseKvhQIAzfX4/banChatMember');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            $response = curl_exec($ch);
            curl_close($ch);

            $post2 = [
                'text' => $bot->getUser()->getUsername().' botó a Villergas por no adivinar el número. Gracias por su colaboración',
                'chat_id' => $chatId,
            ];
            $ch2 = curl_init('https://api.telegram.org/bot152580083:AAHSBQ3p9hX42P0l67M-iKtseKvhQIAzfX4/sendMessage');
            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch2, CURLOPT_POSTFIELDS, $post2);
            $response2 = curl_exec($ch2);
            curl_close($ch2);

            $attachment = new Image(Frase::COMUNICADO);
            $message = OutgoingMessage::create()
                    ->withAttachment($attachment);
            $bot->reply($message);

            if ($bot->getUser()->getUsername() == 'JoseLuque7') {
                $bot->reply('Tu tambièn lárgate ctm');
                $luqueId = 9664495;
                $chatId = -1001137860003;
                $post3 = [
                    'user_id' => $luqueId,
                    'chat_id' => $chatId,
                ];
                $ch3 = curl_init('https://api.telegram.org/bot152580083:AAHSBQ3p9hX42P0l67M-iKtseKvhQIAzfX4/banChatMember');
                curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch3, CURLOPT_POSTFIELDS, $post3);
                $response = curl_exec($ch3);
                curl_close($ch3);
            }
        }
    });
});

$botman->hears('/loquecallalaprensa', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $prensas = Prensa::all();
    $text = '';
    foreach ($prensas as $prensa) {
        $text = $text . $prensa->created_at . ': ' . $prensa->mensaje . PHP_EOL;
    }
    $bot->reply($text);
});
$botman->hears('/loquecallalaprensa@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $prensas = Prensa::all();
    $text = '';
    foreach ($prensas as $prensa) {
        $text = $text . $prensa->created_at . ': ' . $prensa->mensaje . PHP_EOL;
    }
    $bot->reply($text);
});
$botman->hears('/prensacorrupta', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('No sea vergante brin. Use este formato: /prensacorrupta Lo que calla la prensa');
});
$botman->hears('/prensacorrupta@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('No sea vergante brin. Use este formato: /prensacorrupta Lo que calla la prensa');
});
$botman->hears('/prensacorrupta@MorochoBot {frase}', function ($bot, $frase) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    if ($frase) {
        Prensa::create(['mensaje' => $frase]);
        $bot->reply('Actualizada la base de datos de "lo que calla la prensa" con la siguiente noticia: '.$frase);
    }
});

$botman->hears('/prensacorrupta {frase}', function ($bot, $frase) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    if ($frase) {
        Prensa::create(['mensaje' => $frase]);
        $bot->reply('Actualizada la base de datos de "lo que calla la prensa" con la siguiente noticia: '.$frase);
    }
});

$botman->hears('/largatevillergas', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    // if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
    //     $attachment = new Image(Gordo::BANIADO);
    //     $message = OutgoingMessage::create()
    //             ->withAttachment($attachment);
    //     $bot->reply($message);
    //     return;
    // }
    $bot->reply('Baraja ctm');
    $userId = 12757116;
    $chatId = -1001137860003;
    $post = [
        'user_id' => $userId,
        'chat_id' => $chatId,
    ];
    $ch = curl_init('https://api.telegram.org/bot152580083:AAHSBQ3p9hX42P0l67M-iKtseKvhQIAzfX4/banChatMember');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    $response = curl_exec($ch);
    curl_close($ch);
    $attachment = new Image(Frase::COMUNICADO);
    $message = OutgoingMessage::create()
            ->withAttachment($attachment);
    $bot->reply($message);

    $bot->reply('Tu también lárgate ctm');
    $luqueId = 9664495;
    $chatId = -1001137860003;
    $post3 = [
        'user_id' => $luqueId,
        'chat_id' => $chatId,
    ];
    $ch3 = curl_init('https://api.telegram.org/bot152580083:AAHSBQ3p9hX42P0l67M-iKtseKvhQIAzfX4/banChatMember');
    curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch3, CURLOPT_POSTFIELDS, $post3);
    $response = curl_exec($ch3);
    curl_close($ch3);
});

$botman->hears('/largatevillergas@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    // if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
    //     $attachment = new Image(Gordo::BANIADO);
    //     $message = OutgoingMessage::create()
    //             ->withAttachment($attachment);
    //     $bot->reply($message);
    //     return;
    // }
    $bot->reply('Baraja ctm');
    $userId = 12757116;
    $chatId = -1001137860003;
    $post = [
        'user_id' => $userId,
        'chat_id' => $chatId,
    ];
    $ch = curl_init('https://api.telegram.org/bot152580083:AAHSBQ3p9hX42P0l67M-iKtseKvhQIAzfX4/banChatMember');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    $response = curl_exec($ch);
    curl_close($ch);
    $attachment = new Image(Frase::COMUNICADO);
    $message = OutgoingMessage::create()
            ->withAttachment($attachment);
    $bot->reply($message);

    $bot->reply('Tu también lárgate ctm');
    $luqueId = 9664495;
    $chatId = -1001137860003;
    $post3 = [
        'user_id' => $luqueId,
        'chat_id' => $chatId,
    ];
    $ch3 = curl_init('https://api.telegram.org/bot152580083:AAHSBQ3p9hX42P0l67M-iKtseKvhQIAzfX4/banChatMember');
    curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch3, CURLOPT_POSTFIELDS, $post3);
    $response = curl_exec($ch3);
    curl_close($ch3);
});

$botman->hears('/jjgb7', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Audio('https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FGarcia%202022.mp3?alt=media&token=bccc6a9e-fefa-404f-b769-d60a19b81712');
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});

$botman->hears('/jjgb7@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Audio('https://firebasestorage.googleapis.com/v0/b/chatapp-7da00.appspot.com/o/audio%2FGarcia%202022.mp3?alt=media&token=bccc6a9e-fefa-404f-b769-d60a19b81712');
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});

$botman->hears('/gordaso {index}', function ($bot, $index) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $index = intval($index);
    if (is_int($index)) {
        $cerdo = Frase::gordasoIndex($index);
        if ($cerdo === null) {
            $bot->reply('No valga pistolón brin. Existen '.Frase::countGordasos().' fotos de gordasos. Mande un valor del 1 al '.Frase::countGordasos().'.');
        } else {
            $attachment = new Image(Frase::gordasoIndex($index));
            $message = OutgoingMessage::create()
                        ->withAttachment($attachment);
            $bot->reply($message);
        }
    } else {
        $bot->reply('No valga pistolón brin. Existen '.Frase::countGordasos().' fotos de gordasos. Mande un valor del 1 al '.Frase::countGordasos().'.');
    }
});
$botman->hears('/gordaso@MorochoBot {index}', function ($bot, $index) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $index = intval($index);
    if (is_int($index)) {
        $cerdo = Frase::gordasoIndex($index);
        if ($cerdo === null) {
            $bot->reply('No valga pistolón brin. Existen '.Frase::countGordasos().' fotos de gordasos. Mande un valor del 1 al '.Frase::countGordasos().'.');
        } else {
            $attachment = new Image(Frase::gordasoIndex($index));
            $message = OutgoingMessage::create()
                        ->withAttachment($attachment);
            $bot->reply($message);
        }
    } else {
        $bot->reply('No valga pistolón brin. Existen '.Frase::countGordasos().' fotos de gordasos. Mande un valor del 1 al '.Frase::countGordasos().'.');
    }
});
$botman->hears('/gordaso@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Anda valiendo más pistola que Villegol brin. El formato es /gordaso (número entero). Ejemplo: /gordaso 1');
});
$botman->hears('/gordaso', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Anda valiendo más pistola que Villegol brin. El formato es /gordaso (número entero). Ejemplo: /gordaso 1');
});

$botman->hears('aaaaa.*', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::TANGA_VILLERGAS);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Villegas maricón!');
});

$botman->hears('aaaa', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::TANGA_VILLERGAS);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Te falta una A brin');
});

$botman->hears('Morocho', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::TANGA_VILLERGAS);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Audio(Frase::AUDIOS['chupalo']);
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});

$botman->hears('Chupalo', function ($bot) {
    // ChatUser::saveUser($bot->getUser());
    // if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
    //     $attachment = new Image(Gordo::TANGA_VILLERGAS);
    //     $message = OutgoingMessage::create()
    //             ->withAttachment($attachment);
    //     $bot->reply($message);
    //     return;
    // }
    // $bot->reply('Un ratito');
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::TANGA_VILLERGAS);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Audio(Frase::AUDIOS['unratito']);
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});

$botman->hears('Chúpalo', function ($bot) {
    // ChatUser::saveUser($bot->getUser());
    // if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
    //     $attachment = new Image(Gordo::TANGA_VILLERGAS);
    //     $message = OutgoingMessage::create()
    //             ->withAttachment($attachment);
    //     $bot->reply($message);
    //     return;
    // }
    // $bot->reply('Un ratito');
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::TANGA_VILLERGAS);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Audio(Frase::AUDIOS['unratito']);
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});

$botman->hears('Un ratito', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::TANGA_VILLERGAS);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Morocho');
});

$botman->hears('Mijita', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::TANGA_VILLERGAS);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Maldita');
});

$botman->hears('Maldita', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::TANGA_VILLERGAS);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Mijita');
});

$botman->hears('/testing', function ($bot) {
    ChatUser::saveUser($bot->getUser());
});

$botman->hears('/audio {index}', function ($bot, $index) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $audio = Frase::AUDIOS[$index] ?? null;
    if ($audio) {
        $attachment = new Audio($audio);
        $message = OutgoingMessage::create()
                    ->withAttachment($attachment);
        $bot->reply($message);
    } else {
        $audioOptions = array_keys(Frase::AUDIOS);
        sort($audioOptions);
        $bot->reply('No valga pistolón brin. Estas son las opciones de audio: '
        .implode(', ', $audioOptions));
    }
});
$botman->hears('/audio@MorochoBot {index}', function ($bot, $index) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $audio = Frase::AUDIOS[$index] ?? null;
    if ($audio) {
        $attachment = new Audio($audio);
        $message = OutgoingMessage::create()
                    ->withAttachment($attachment);
        $bot->reply($message);
    } else {
        $audioOptions = array_keys(Frase::AUDIOS);
        sort($audioOptions);
        $bot->reply('No valga pistolón brin. Estas son las opciones de audio: '
        .implode(', ', $audioOptions));
    }
});
$botman->hears('/audio@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $audioOptions = array_keys(Frase::AUDIOS);
    sort($audioOptions);
    $bot->reply('Anda valiendo más pistola que Villegol brin. El formato es /audio (audio). Estas son las opciones de audio: '
    .implode(', ', $audioOptions));
});
$botman->hears('/audio', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $audioOptions = array_keys(Frase::AUDIOS);
    sort($audioOptions);
    $bot->reply('Anda valiendo más pistola que Villegol brin. El formato es /audio (audio). Estas son las opciones de audio: '
    .implode(', ', $audioOptions));
});

$botman->hears('/tanga@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Image(Gordo::TANGA_VILLERGAS);
    $message = OutgoingMessage::create()
            ->withAttachment($attachment);
    $bot->reply($message);
});

$botman->hears('/tanga', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Image(Gordo::TANGA_VILLERGAS);
    $message = OutgoingMessage::create()
            ->withAttachment($attachment);
    $bot->reply($message);
});

$botman->hears('/villergas@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('EN EL ORTO LA TIENES VILLERGAS CHUCHA TU MADRE.');
});

$botman->hears('/villergas', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('EN EL ORTO LA TIENES VILLERGAS CHUCHA TU MADRE.');
});

$botman->hears('/opinebrin@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply(Frase::inRandomOrder()->first()->mensaje);
});
$botman->hears('/opinebrin', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply(Frase::inRandomOrder()->first()->mensaje);
});
$botman->hears('/morocho@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply(Frase::inRandomOrder()->first()->mensaje);
});
$botman->hears('/morocho', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply(Frase::inRandomOrder()->first()->mensaje);
});
$botman->hears('/user', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply($bot->getUser()->getId().' '.$bot->getUser()->getUsername());
});
$botman->hears('/frase', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('No sea vergante brin. Use este formato: /frase Lo que quiere que opine El Morocho');
});
$botman->hears('/frase@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('No sea vergante brin. Use este formato: /frase Lo que quiere que opine El Morocho');
});
$botman->hears('/frasechapo {frase}', function ($bot, $frase) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    if ($frase) {
        Chapo::create(['mensaje' => $frase]);
        $bot->reply('Chapo va a opinar "'.$frase.'" de ahora en adelante brin');
    }
});
$botman->hears('/frasechapo@MorochoBot {frase}', function ($bot, $frase) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    if ($frase) {
        Chapo::create(['mensaje' => $frase]);
        $bot->reply('Chapo va a opinar "'.$frase.'" de ahora en adelante brin');
    }
});
$botman->hears('/frasechapo', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('No sea vergante brin. Use este formato: /frasechapo Lo que quiere que opine Chapo');
});
$botman->hears('/frasechapo@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('No sea vergante brin. Use este formato: /frasechapo Lo que quiere que opine Chapo');
});
$botman->hears('/chapo', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply(Chapo::inRandomOrder()->first()->mensaje);
});
$botman->hears('/chapo@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply(Chapo::inRandomOrder()->first()->mensaje);
});
$botman->hears('/nuevapelea {frase}', function ($bot, $frase) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    if ($frase) {
        Gordo::create(['mensaje' => $frase]);
        $bot->reply('Se almacenó la puteada entre gordos: "'.$frase.'"');
    }
});
$botman->hears('/nuevapelea@MorochoBot {frase}', function ($bot, $frase) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    if ($frase) {
        Gordo::create(['mensaje' => $frase]);
        $bot->reply('Se almacenó la puteada entre gordos: "'.$frase.'"');
    }
});
$botman->hears('/nuevapelea', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('No sea inculto plixz. Use este formato: /nuevapelea La puteada entre gordos.');
});
$botman->hears('/nuevapelea@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('No sea inculto plixz. Use este formato: /nuevapelea La puteada entre gordos.');
});
$botman->hears('/peleadegordos', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply(Gordo::inRandomOrder()->first()->mensaje);
});
$botman->hears('/peleadegordos@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply(Gordo::inRandomOrder()->first()->mensaje);
});

$botman->hears('/frase {frase}', function ($bot, $frase) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    if ($frase) {
        Frase::create(['mensaje' => $frase]);
        $bot->reply('El Morocho va a opinar "'.$frase.'" de ahora en adelante brin');
    }
});
$botman->hears('/frase@MorochoBot {frase}', function ($bot, $frase) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    if ($frase) {
        Frase::create(['mensaje' => $frase]);
        $bot->reply('El Morocho va a opinar "'.$frase.'" de ahora en adelante brin');
    }
});
$botman->hears('/frasegersy {frase}', function ($bot, $frase) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    if ($frase) {
        Gersy::create(['mensaje' => $frase]);
        $bot->reply('Gersy va a opinar "'.$frase.'" de ahora en adelante brin');
    }
});
$botman->hears('/frasegersy@MorochoBot {frase}', function ($bot, $frase) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    if ($frase) {
        Gersy::create(['mensaje' => $frase]);
        $bot->reply('Gersy va a opinar "'.$frase.'" de ahora en adelante brin');
    }
});
$botman->hears('/frasegersy', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('No sea vergante brin. Use este formato: /frasegersy Lo que quiere que opine Gersy');
});
$botman->hears('/frasegersy@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('No sea vergante brin. Use este formato: /frasegersy Lo que quiere que opine Gersy');
});
$botman->hears('/gersy', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply(Gersy::inRandomOrder()->first()->mensaje);
});
$botman->hears('/gersy@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply(Gersy::inRandomOrder()->first()->mensaje);
});
$botman->hears('/ultima@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply(Frase::orderBy('id', 'DESC')->first()->mensaje);
});
$botman->hears('/ultima', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply(Frase::orderBy('id', 'DESC')->first()->mensaje);
});
$botman->hears('/negrocerdo@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Image(Frase::randomCerdo());
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});
$botman->hears('/negrocerdo', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Image(Frase::randomCerdo());
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});
$botman->hears('/blacklivesmatter@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Image(Frase::randomBlackLivesMatter());
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});
$botman->hears('/blacklivesmatter', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Image(Frase::randomBlackLivesMatter());
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});
$botman->hears('/ultimocerdo@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Image(Frase::lastCerdo());
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});
$botman->hears('/ultimocerdo', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $attachment = new Image(Frase::lastCerdo());
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});
$botman->hears('/negrocerdo2 {index}', function ($bot, $index) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $index = intval($index);
    if (is_int($index)) {
        $cerdo = Frase::cerdoIndex($index);
        if ($cerdo === null) {
            $bot->reply('No valga pistolón brin. Existen '.Frase::countCerdos().' fotos del negro cerdo. Mande un valor del 1 al '.Frase::countCerdos().'.');
        } else {
            $attachment = new Image(Frase::cerdoIndex($index));
            $message = OutgoingMessage::create()
                        ->withAttachment($attachment);
            $bot->reply($message);
        }
    } else {
        $bot->reply('No valga pistolón brin. Existen '.Frase::countCerdos().' fotos del negro cerdo. Mande un valor del 1 al '.Frase::countCerdos().'.');
    }
});
$botman->hears('/negrocerdo2@MorochoBot {index}', function ($bot, $index) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $index = intval($index);
    if (is_int($index)) {
        $cerdo = Frase::cerdoIndex($index);
        if ($cerdo === null) {
            $bot->reply('No valga pistolón brin. Existen '.Frase::countCerdos().' fotos del negro cerdo. Mande un valor del 1 al '.Frase::countCerdos().'.');
        } else {
            $attachment = new Image(Frase::cerdoIndex($index));
            $message = OutgoingMessage::create()
                        ->withAttachment($attachment);
            $bot->reply($message);
        }
    } else {
        $bot->reply('No valga pistolón brin. Existen '.Frase::countCerdos().' fotos del negro cerdo. Mande un valor del 1 al '.Frase::countCerdos().'.');
    }
});
$botman->hears('/negrocerdo2@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Anda valiendo más pistola que Villegol brin. El formato es /negrocerdo2 (número entero). Ejemplo: /negrocerdo2 10');
});
$botman->hears('/negrocerdo2', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Anda valiendo más pistola que Villegol brin. El formato es /negrocerdo2 (número entero). Ejemplo: /negrocerdo2 10');
});
$botman->hears('/garciamedebe {deuda}', function ($bot, $deuda) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $valor = floatval($deuda);
    if ($valor > 0) {
        $garcia = Garcia::inRandomOrder()->first();
        $garcia->deuda = $garcia->deuda + $valor;
        $garcia->save();
        $garcia = Garcia::inRandomOrder()->first();
        $bot->reply('García debe un total de $'.$garcia->deuda.'. #PagaGarcia');
    } else {
        $bot->reply('Deje de valer pistola brin. El formato es /garciamedebe (deuda en dólares). Ejemplo: /garciamedebe 2.50');
    }
});
$botman->hears('/garciamedebe', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Deje de valer pistola brin. El formato es /garciamedebe (deuda en dólares). Ejemplo: /garciamedebe 2.50');
});
$botman->hears('/garciamedebe@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Deje de valer pistola brin. El formato es /garciamedebe (deuda en dólares). Ejemplo: /garciamedebe 2.50');
});
$botman->hears('/garciamedebe@MorochoBot {deuda}', function ($bot, $deuda) {
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $valor = floatval($deuda);
    if ($valor > 0) {
        $garcia = Garcia::inRandomOrder()->first();
        $garcia->deuda = $garcia->deuda + $valor;
        $garcia->save();
        $garcia = Garcia::inRandomOrder()->first();
        $bot->reply('García debe un total de $'.round($garcia->deuda, 2).'. #PagaGarcia');
    } else {
        $bot->reply('Deje de valer pistola brin. El formato es /garciamedebe (deuda en dólares). Ejemplo: /garciamedebe 2.50');
    }
});
$botman->hears('/pagagarcia@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Que quieres que no coma?');
});
$botman->hears('/pagagarcia', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $bot->reply('Que quieres que no coma?');
});
$botman->hears('/garcia@MorochoBot', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $garcia = [
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/32428_422419465521_1802163_n.jpg?_nc_cat=109&_nc_ht=scontent.fgye1-1.fna&oh=cd9494f66e9989c7b8a9eaa12a7b1edd&oe=5D4FA21B',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/1913941_152488740521_727384_n.jpg?_nc_cat=104&_nc_ht=scontent.fgye1-1.fna&oh=e297ab1e2a6544a149ed50a7adafe6ed&oe=5D3F6B43',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/10982611_10153495646470522_798555186440004510_n.jpg?_nc_cat=107&_nc_ht=scontent.fgye1-1.fna&oh=b84a34127bf0fd98171994499c3899f8&oe=5D3D9CFC',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/487272_10151072273380522_1996759927_n.jpg?_nc_cat=107&_nc_ht=scontent.fgye1-1.fna&oh=303b1436c676e024e63b53a421e3f652&oe=5D433221',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/1918089_188149990521_6683272_n.jpg?_nc_cat=110&_nc_ht=scontent.fgye1-1.fna&oh=b921665ac30d50f2ff4b9643a5235149&oe=5D415911',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/183280_10150150577300522_161518_n.jpg?_nc_cat=104&_nc_ht=scontent.fgye1-1.fna&oh=ec330a379ed290b3f9b7bdd1bd26d736&oe=5D3EDA11',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/10346198_10152661744650522_6889074251422990166_n.jpg?_nc_cat=104&_nc_ht=scontent.fgye1-1.fna&oh=e71b7f0c9b78347fee1ea6f5f1da0fcb&oe=5D4203E5',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/11825128_10153554587775522_3053187471567391405_n.jpg?_nc_cat=111&_nc_ht=scontent.fgye1-1.fna&oh=3aada2433285240465210f1b525e4368&oe=5D04667C',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/r90/11949443_10153619716800522_7218984951459886938_n.jpg?_nc_cat=111&_nc_ht=scontent.fgye1-1.fna&oh=ddf9d2b334ca70dd3c634e92e65e4991&oe=5D4E8552',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/r90/11709571_10153619716835522_4546783521684550061_n.jpg?_nc_cat=108&_nc_ht=scontent.fgye1-1.fna&oh=f6a523cdadf080aae40be20c2a30960f&oe=5D05D18A',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/10377992_10152661748745522_8649915012667211070_n.jpg?_nc_cat=111&_nc_ht=scontent.fgye1-1.fna&oh=0c736d52f7b588e3ddf7ec395a574432&oe=5D330B45'
    ];
    shuffle($garcia);
    $attachment = new Image($garcia[0]);
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});
$botman->hears('/garcia', function ($bot) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $garcia = [
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/32428_422419465521_1802163_n.jpg?_nc_cat=109&_nc_ht=scontent.fgye1-1.fna&oh=cd9494f66e9989c7b8a9eaa12a7b1edd&oe=5D4FA21B',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/1913941_152488740521_727384_n.jpg?_nc_cat=104&_nc_ht=scontent.fgye1-1.fna&oh=e297ab1e2a6544a149ed50a7adafe6ed&oe=5D3F6B43',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/10982611_10153495646470522_798555186440004510_n.jpg?_nc_cat=107&_nc_ht=scontent.fgye1-1.fna&oh=b84a34127bf0fd98171994499c3899f8&oe=5D3D9CFC',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/487272_10151072273380522_1996759927_n.jpg?_nc_cat=107&_nc_ht=scontent.fgye1-1.fna&oh=303b1436c676e024e63b53a421e3f652&oe=5D433221',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/1918089_188149990521_6683272_n.jpg?_nc_cat=110&_nc_ht=scontent.fgye1-1.fna&oh=b921665ac30d50f2ff4b9643a5235149&oe=5D415911',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/183280_10150150577300522_161518_n.jpg?_nc_cat=104&_nc_ht=scontent.fgye1-1.fna&oh=ec330a379ed290b3f9b7bdd1bd26d736&oe=5D3EDA11',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/10346198_10152661744650522_6889074251422990166_n.jpg?_nc_cat=104&_nc_ht=scontent.fgye1-1.fna&oh=e71b7f0c9b78347fee1ea6f5f1da0fcb&oe=5D4203E5',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/11825128_10153554587775522_3053187471567391405_n.jpg?_nc_cat=111&_nc_ht=scontent.fgye1-1.fna&oh=3aada2433285240465210f1b525e4368&oe=5D04667C',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/r90/11949443_10153619716800522_7218984951459886938_n.jpg?_nc_cat=111&_nc_ht=scontent.fgye1-1.fna&oh=ddf9d2b334ca70dd3c634e92e65e4991&oe=5D4E8552',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/r90/11709571_10153619716835522_4546783521684550061_n.jpg?_nc_cat=108&_nc_ht=scontent.fgye1-1.fna&oh=f6a523cdadf080aae40be20c2a30960f&oe=5D05D18A',
        'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/10377992_10152661748745522_8649915012667211070_n.jpg?_nc_cat=111&_nc_ht=scontent.fgye1-1.fna&oh=0c736d52f7b588e3ddf7ec395a574432&oe=5D330B45'
    ];
    shuffle($garcia);
    $attachment = new Image($garcia[0]);
    $message = OutgoingMessage::create()
                ->withAttachment($attachment);
    $bot->reply($message);
});

$botman->hears('/botaravillergascadaminuto {index}', function ($bot, $index) {
    ChatUser::saveUser($bot->getUser());
    if ($bot->getUser()->getId() == Gordo::VILLERGAS_ID) {
        $attachment = new Image(Gordo::BANIADO);
        $message = OutgoingMessage::create()
                ->withAttachment($attachment);
        $bot->reply($message);
        return;
    }
    $config = Configuracion::first();
    if (!$config) {
        $config = Configuracion::create([
            'botar_a_villergas_cada_min' => 0
        ]);
    }
    $index = intval($index);
    if (!is_int($index)) {
        $bot->reply('No valga pistolón brin. Debe enviar 1 o 0 para activar o desactivar la huevada. Es preferible dejarlo activo.');
    }
    if ($index == 1) {
        $config->botar_a_villergas_cada_min = true;
        $config->save();
        $bot->reply('Botar a Villergas cada minuto: Activado. Gracias por su colaboración.');
    }
    if ($index == 0) {
        $config->botar_a_villergas_cada_min = false;
        $config->save();
        $bot->reply('Botar a Villergas cada minuto: Desactivado. HASTA CUANDO CHUCHA!');
    }
    if ($index > 1) {
        $bot->reply('No valga pistolón brin. Debe enviar 1 o 0 para activar o desactivar la huevada. Es preferible dejarlo activo.');
    }
});

// $botman->hears('Start conversation', BotManController::class.'@startConversation');
